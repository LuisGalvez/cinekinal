package com.example.luisgalvez.loginlc;

import android.support.v4.app.Fragment;
import android.content.Context;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import java.util.ArrayList;
import java.util.List;

import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.Response;


public class HomeFragment extends android.app.Fragment {
    public ListView lvPrestrenos;
    private SQLiteDatabase db;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.item_prestrenos, container, false);
        lvPrestrenos = (ListView) view.findViewById(R.id.fecha_apertura);
        RestAdapter restAdapter = new RestAdapter.Builder().setEndpoint("http://192.168.1.32/cinekinal2013303/public").build();
        HomeService service = restAdapter.create(HomeService.class);
        service.getPrestreno(new Callback<List<Prestrenos>>() {
            @Override
            public void success(List<Prestrenos> prestrenoses, Response response) {
                List<Prestrenos> prestrenoses1 = new ArrayList<Prestrenos>();
                prestrenoses = prestrenoses1;
                AdaptadorPrestrenos adaptadorPrestrenos = new AdaptadorPrestrenos(getActivity(), prestrenoses1);
                lvPrestrenos.setAdapter(adaptadorPrestrenos);
            }

            @Override
            public void failure(RetrofitError retrofitError) {
                Log.e("Failure error ", String.valueOf(retrofitError));
            }
        });
        lvPrestrenos.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
                Prestrenos prestrenos = (Prestrenos) parent.getItemAtPosition(position);

                return false;
            }
        });
        lvPrestrenos.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Prestrenos Prestrenos = (Prestrenos) parent.getItemAtPosition(position);
                Prestrenos.getInstancia().setId(Prestrenos.getId());
                Prestrenos.getInstancia().setPelicula_id(Prestrenos.getPelicula_id());
                Prestrenos.getInstancia().setFecha_apertura(Prestrenos.getFecha_apertura());
                Prestrenos.getInstancia().setFecha_cierre(Prestrenos.getFecha_cierre());

                startActivity(new Intent(getActivity(), TitularActivity.class));
            }
        });
        return view;
    }

    public class AdaptadorPrestrenos extends ArrayAdapter<Prestrenos> {
        private List<Prestrenos> listaPrestrenos;

        public AdaptadorPrestrenos(Context context, List<Prestrenos> listaPrestrenos) {
            super(context, R.layout.item_prestrenos, listaPrestrenos);
            listaPrestrenos = listaPrestrenos;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            LayoutInflater layoutInflater = LayoutInflater.from(getContext());
            View item = layoutInflater.inflate(R.layout.item_prestrenos, null);

            ImageView imageView = (ImageView) item.findViewById(R.id.imagen);
            TextView apertura = (TextView) item.findViewById(R.id.fecha_apertura);
            TextView cierre = (TextView) item.findViewById(R.id.fecha_cierre);
            //Picasso.with(getContext()).load(listaPeliculas.get(position).getImage()).resize(100, 100).into(imageView);
            apertura.setText(listaPrestrenos.get(position).getId());
            cierre.setText("Id: "+listaPrestrenos.get(position).getId());

            return item;
        }
    }
}