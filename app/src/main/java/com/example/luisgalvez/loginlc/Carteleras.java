package com.example.luisgalvez.loginlc;

import java.util.Date;

/**
 * Created by ALUMNO on 03/08/2015.
 */
public class Carteleras {

    public int id;
    public int sala_id;
    public int pelicula_id;
    public int formatopelicula_id;
    public String formato_lenguaje;
    public Date fecha;
    public Date hora;
    public static Carteleras instancia;

    public static Carteleras getInstancia(){
        if(instancia==null){
            instancia= new Carteleras();
        }
        return  instancia;
    }


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getSala_id() {
        return sala_id;
    }

    public void setSala_id(int sala_id) {
        this.sala_id = sala_id;
    }

    public int getPelicula_id() {
        return pelicula_id;
    }

    public void setPelicula_id(int pelicula_id) {
        this.pelicula_id = pelicula_id;
    }

    public int getFormatopelicula_id() {
        return formatopelicula_id;
    }

    public void setFormatopelicula_id(int formatopelicula_id) {
        this.formatopelicula_id = formatopelicula_id;
    }

    public String getFormato_lenguaje() {
        return formato_lenguaje;
    }

    public void setFormato_lenguaje(String formato_lenguaje) {
        this.formato_lenguaje = formato_lenguaje;
    }

    public Date getFecha() {
        return fecha;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }

    public Date getHora() {
        return hora;
    }

    public void setHora(Date hora) {
        this.hora = hora;
    }

    public static void setInstancia(Carteleras instancia) {
        Carteleras.instancia = instancia;
    }

    @Override
    public String toString() {
        return "Carteleras{" +
                "id=" + id +
                ", sala_id=" + sala_id +
                ", pelicula_id=" + pelicula_id +
                ", formatopelicula_id=" + formatopelicula_id +
                ", formato_lenguaje='" + formato_lenguaje + '\'' +
                ", fecha=" + fecha +
                ", hora=" + hora +
                '}';
    }
}
