package com.example.luisgalvez.loginlc;
import android.content.ContentValues;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.parse.ParseException;
import com.parse.ParseUser;
import com.parse.SignUpCallback;

public class registrar extends ActionBarActivity {
    EditText txtUsuarioRegistrar,txtPassRegistrar,txtPassVerificar, txtUsuarioCorreo,edtCorreo ;
    Button btnRegistrarUsuario;
    RelativeLayout rl;
    public SQLiteDatabase sqLiteDatabase;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registrar);
        rl = (RelativeLayout)findViewById(R.id.registrar);
        btnRegistrarUsuario= (Button) findViewById(R.id.btnRegistrarUsuario);
        txtUsuarioRegistrar = (EditText) findViewById(R.id.txtUsuarioRegistrar);
        txtPassRegistrar = (EditText) findViewById(R.id.txtPassRegistrar);
        txtPassVerificar = (EditText) findViewById(R.id.txtVerificarPass);
        txtUsuarioCorreo = (EditText)findViewById(R.id.editTextNombre);
        edtCorreo = (EditText)findViewById(R.id.editTextCorreo);
        Bundle miBundle = this.getIntent().getExtras();
        try{
            btnRegistrarUsuario.setOnClickListener(new View.OnClickListener() {
                public void onClick(View view) {
                    if (txtUsuarioRegistrar.getText().toString().matches("") || txtPassRegistrar.getText().toString().matches("") || txtPassVerificar.getText().toString().matches("")) {
                        Toast toast = Toast.makeText(getApplicationContext(), "Ingrese correctamente los datos", Toast.LENGTH_LONG);
                        toast.show();
                    } else if (txtPassRegistrar.getText().toString().matches(txtPassVerificar.getText().toString())) {
                        ParseUser user = new ParseUser();
                        user.setUsername(txtUsuarioRegistrar.getText().toString()+"");
                        user.setPassword(txtPassVerificar.getText().toString()+"");
                        user.setEmail(edtCorreo.getText().toString()+"");
                        user.signUpInBackground(new SignUpCallback() {
                            public void done(ParseException e) {
                                if (e == null) {
                                    // Show a simple Toast message upon successful registration
                                    Toast.makeText(getApplicationContext(),
                                            "Successfully Signed up, please log in.",
                                            Toast.LENGTH_LONG).show();
                                } else {
                                    Log.e("Error login", String.valueOf(e));
                                    Toast.makeText(getApplicationContext(),
                                            "Sign up Error", Toast.LENGTH_LONG)
                                            .show();
                                }
                            }
                        });

                    } else if (txtPassVerificar.getText().toString() != txtPassVerificar.getText().toString()) {
                        Toast t = Toast.makeText(getApplicationContext(), "Las Contraseñas no son iguales", Toast.LENGTH_SHORT);
                        t.show();
                    }
                }
            });
        }catch (NullPointerException e){
        }
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_registrar, menu);
        return true;
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
}