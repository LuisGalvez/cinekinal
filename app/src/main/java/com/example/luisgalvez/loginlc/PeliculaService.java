package com.example.luisgalvez.loginlc;

import java.util.List;

import retrofit.Callback;
import retrofit.http.GET;

/**
 * Created by Luis Galvez on 25/07/2015.
 */
public interface PeliculaService {
    @GET("/peliculas")
    void getPelicula(Callback<List<Peliculas>> callback);
}
