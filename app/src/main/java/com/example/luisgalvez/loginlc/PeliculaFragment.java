package com.example.luisgalvez.loginlc;


import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;

import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.Response;


public class PeliculaFragment extends Fragment {


    public ListView lvPelicula;
    private SQLiteDatabase db;

    public PeliculaFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_pelicula, container, false);
        lvPelicula = (ListView) view.findViewById(R.id.lvPelicula);
        RestAdapter restAdapter = new RestAdapter.Builder().setEndpoint("http://192.168.1.32/cinekinal2013303/public").build();
        PeliculaService service = restAdapter.create(PeliculaService.class);
        service.getPelicula(new Callback<List<Peliculas>>() {
            @Override
            public void success(List<Peliculas> peliculas, Response response) {
                List<Peliculas> peliculas2 = new ArrayList<Peliculas>();
                peliculas2 = peliculas;
                AdaptadorPeliculas adaptadorPeliculas = new AdaptadorPeliculas(getActivity(), peliculas2);
                lvPelicula.setAdapter(adaptadorPeliculas);
            }

            @Override
            public void failure(RetrofitError retrofitError) {
                Log.e("Failure error ", String.valueOf(retrofitError));
            }
        });
        lvPelicula.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
                Peliculas pelicula = (Peliculas) parent.getItemAtPosition(position);
                /*Boolean agregar = true;
                PeliculasHelper peliculaDB =
                        new PeliculasHelper(getActivity(), "Peliculas", null, 1);
                db = peliculaDB.getWritableDatabase();
                Cursor c = db.rawQuery("SELECT idPelicula, titulo, sinopsis, trailer_url, image, fechaEstreno, rated, genero FROM Pelicula", null);
                if (c.moveToFirst()) {
                    do {
                        if (c.getInt(0) == pelicula.getId()) {
                            agregar = false;
                        }
                    } while (c.moveToNext());
                }
                if (agregar == true) {
                    ContentValues nuevoRegistro = new ContentValues();
                    nuevoRegistro.put("idPelicula", pelicula.id + "");
                    nuevoRegistro.put("titulo", pelicula.titulo + "");
                    nuevoRegistro.put("sinopsis", pelicula.sinopsis + "");
                    nuevoRegistro.put("trailer_url", pelicula.trailer_url + "");
                    nuevoRegistro.put("image", pelicula.image + "");
                    nuevoRegistro.put("rated", pelicula.rated + "");
                    nuevoRegistro.put("genero", pelicula.genero + "");
                    Log.e("Javier", pelicula.id + " " + pelicula.titulo + " " + pelicula.sinopsis + " " + pelicula.trailer_url + " "
                            + pelicula.image + " "  + pelicula.rated + " " + pelicula.genero + " ");
                    db.insert("Pelicula", null, nuevoRegistro);
                    Toast.makeText(getActivity(), "Agregado a favoritos", Toast.LENGTH_SHORT).show();
                } else {
                    Integer[] args = new Integer[]{pelicula.id};
                    db.execSQL("DELETE FROM Pelicula WHERE idPelicula = ?", args);
                    Toast.makeText(getActivity(), "Eliminado de favoritos", Toast.LENGTH_SHORT).show();
                }
*/
                return false;
            }
        });
        lvPelicula.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Peliculas Peliculas = (Peliculas) parent.getItemAtPosition(position);
                Peliculas.getInstancia().setId(Peliculas.getId());
                Peliculas.getInstancia().setTitulo(Peliculas.getTitulo());
                Peliculas.getInstancia().setSinopsis(Peliculas.getSinopsis());
                Peliculas.getInstancia().setTrailer_url(Peliculas.getTrailer_url());
                Peliculas.getInstancia().setRated(Peliculas.getRated());
                Peliculas.getInstancia().setGenero(Peliculas.getGenero());
                Peliculas.getInstancia().setImage(Peliculas.getImage());

                startActivity(new Intent(getActivity(), TitularActivity.class));
            }
        });
        return view;
    }

    public class AdaptadorPeliculas extends ArrayAdapter<Peliculas> {
        private List<Peliculas> listaPeliculas;

        public AdaptadorPeliculas(Context context, List<Peliculas> listaPelicula) {
            super(context, R.layout.item_peliculas, listaPelicula);
            listaPeliculas = listaPelicula;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            LayoutInflater layoutInflater = LayoutInflater.from(getContext());
            View item = layoutInflater.inflate(R.layout.item_peliculas, null);

            ImageView imageView = (ImageView) item.findViewById(R.id.imagen);
            TextView titulo = (TextView) item.findViewById(R.id.titulo);
            TextView genero = (TextView) item.findViewById(R.id.genero);
            TextView rated = (TextView) item.findViewById(R.id.rated);
            //Picasso.with(getContext()).load(listaPeliculas.get(position).getImage()).resize(100, 100).into(imageView);
            titulo.setText(listaPeliculas.get(position).getTitulo());
            genero.setText("Genero: "+listaPeliculas.get(position).getGenero());
            rated.setText("Rated: "+listaPeliculas.get(position).getRated());

            return item;
        }
    }
}