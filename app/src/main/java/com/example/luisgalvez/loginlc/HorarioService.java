package com.example.luisgalvez.loginlc;

import java.util.List;

import retrofit.Callback;
import retrofit.http.GET;


public interface HorarioService {
    @GET("/carteleras")
    void getCartelera(Callback<List<Carteleras>> callback);
}