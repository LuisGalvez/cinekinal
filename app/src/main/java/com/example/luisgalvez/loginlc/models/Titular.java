package com.example.luisgalvez.loginlc.models;

/**
 * Created by LuisGalvez on 26/05/2015.
 */
public class Titular {
    private String titulo;
    private String subtitulo;
    private String descripcion;
    public int favoritos;


    public String getTitulo() {
        return titulo;
    }

    public String getSubtitulo() {
        return subtitulo;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public int getFavoritos() {
        return favoritos;
    }

    public Titular(String titulo, String subtitulo, String descripcion, int favoritos) {
        this.titulo = titulo;
        this.subtitulo = subtitulo;
        this.descripcion = descripcion;
        this.favoritos = favoritos;
    }
}
