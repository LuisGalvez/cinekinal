package com.example.luisgalvez.loginlc;

import java.util.Date;

/**
 * Created by ALUMNO on 29/07/2015.
 */
public class Prestrenos {

    private int id;
    private int pelicula_id;
    private Date fecha_apertura;
    private Date fecha_cierre;
    public static Prestrenos instancia;

    public static Prestrenos getInstancia(){
        if(instancia==null){
            instancia= new Prestrenos();
        }
        return  instancia;
    }

    public Prestrenos() {
        this.id = id;
        this.pelicula_id = pelicula_id;
        this.fecha_apertura = fecha_apertura;
        this.fecha_cierre = fecha_cierre;
    }


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getPelicula_id() {
        return pelicula_id;
    }

    public void setPelicula_id(int pelicula_id) {
        this.pelicula_id = pelicula_id;
    }

    public Date getFecha_apertura() {
        return fecha_apertura;
    }

    public void setFecha_apertura(Date fecha_apertura) {
        this.fecha_apertura = fecha_apertura;
    }

    public Date getFecha_cierre() {
        return fecha_cierre;
    }

    public void setFecha_cierre(Date fecha_cierre) {
        this.fecha_cierre = fecha_cierre;
    }

    @Override
    public String toString() {
        return "Home{" +
                "id=" + id +
                ", pelicula_id=" + pelicula_id +
                ", fecha_apertura=" + fecha_apertura +
                ", fecha_cierre=" + fecha_cierre +
                '}';
    }
}
