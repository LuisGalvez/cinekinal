package com.example.luisgalvez.loginlc;

import android.content.Intent;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.Fragment;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.parse.ParseUser;


public class pelicula extends ActionBarActivity implements NavigationDrawerFragment.FragmentDrawerListener {

    private Toolbar mToolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pelicula);
        mToolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(mToolbar);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        NavigationDrawerFragment drawerFragment = (NavigationDrawerFragment) getSupportFragmentManager().findFragmentById(R.id.navigation_drawer_fragment);

        drawerFragment.setUp((DrawerLayout) findViewById(R.id.drawer_layout), mToolbar, R.id.navigation_drawer_fragment);
        drawerFragment.setDrawerListener(this);

        //UsersSQLiteHelper usersSQLiteHelper = new UsersSQLiteHelper(this, "DBCine", null, 1);
        //SQLiteDatabase db = usersSQLiteHelper.getWritableDatabase();

//        if (db != null){
           // for (int i = 0; i < 10; i++){
           //     String nombre = "Usuario " + i;

           //    db.execSQL("INSERT INTO Usuarios VALUES (null, '" + nombre + "')");
           // }

           //Insertar
           // ContentValues nuevoRegistro = new ContentValues();
           // nuevoRegistro.put("nombre", "lgalvez");
           //db.insert("Usuarios", null, nuevoRegistro);

            // Actualizar
            // ContentValues valores = new ContentValues();
            //valores.put("nombre", "luisgalvez");
            //db.update("Usuarios", valores, "id=1", null);

            //Eliminar
           // db.delete("Usuarios", "id=3", null);


            //db.close();

            //Cursor c = db.rawQuery("SELECT id, nombre FROM Usuarios", null );
            //if(c.moveToFirst()){
              //  do{
                //    mHello.append("\n id:  " + c.getInt(0) + " nombre: " + c.getString(1) + "\n");
                //}while (c.moveToNext());
            //}
        }


        @Override
        public boolean onCreateOptionsMenu (Menu menu){
            // Inflate the menu; this adds items to the action bar if it is present.
            getMenuInflater().inflate(R.menu.menu_pelicula, menu);
            return true;
        }

        @Override
        public boolean onOptionsItemSelected (MenuItem item){
            // Handle action bar item clicks here. The action bar will
            // automatically handle clicks on the Home/Up button, so long
            // as you specify a parent activity in AndroidManifest.xml.
            int id = item.getItemId();

            if (id == R.id.action_settings) {
            return true;
        }

            if (id == R.id.action_cerrarSesion){
                ParseUser.logOut();
                Intent cerrarsesionIntent = new Intent(pelicula.this, MainActivity.class);
                startActivity(cerrarsesionIntent);
                finish();
                return true;
            }
            if (id == R.id.action_Perfil){

                return true;
            }

            return super.onOptionsItemSelected(item);
        }


        @Override
        public void onDrawerItemSelected (View view,int position){
            Fragment fragment = null;
            HomeFragment fragment1 = null;
            HomeFragment fragment2 = null;
            String title = getString(R.string.app_name);

            switch (position) {
                case 1:
                    title = "Peliculas";
                    fragment = new PeliculaFragment();
                    Toast.makeText(this, "Click en la posicion: " + position, Toast.LENGTH_LONG).show();
                    break;
                case 2:
                    title = "Home";
                    fragment1 = new HomeFragment();
                    Toast.makeText(this, "Click en la posicion: " + position, Toast.LENGTH_LONG).show();
                    break;
                case 3:
                    title = "Favoritos";
                    fragment = new PeliculaFragment();
                    Toast.makeText(this, "Click en la posicion: " + position, Toast.LENGTH_LONG).show();

                    break;
                case 4:
                    title = "Horarios";
                    fragment2 = new HomeFragment();
                    Toast.makeText(this, "Click en la posicion: " + position, Toast.LENGTH_LONG).show();

                    break;
                case 5:
                    title = "Ajustes";
                    fragment1 = new HomeFragment();
                    Toast.makeText(this, "Click en la posicion: " + position, Toast.LENGTH_LONG).show();

                    break;
                default:
                    break;
            }

            if (fragment != null) {
                FragmentManager fragmentManager = getSupportFragmentManager();
                FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                fragmentTransaction.replace(R.id.ContainerLayout, fragment);
                fragmentTransaction.commit();
                getSupportActionBar().setTitle(title);
            }
        }
    }

