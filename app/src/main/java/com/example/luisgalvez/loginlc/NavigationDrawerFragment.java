package com.example.luisgalvez.loginlc;


import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.support.v7.widget.Toolbar;
import android.widget.Toast;

import com.example.luisgalvez.loginlc.adapters.NavigationDrawerAdapter;
import com.example.luisgalvez.loginlc.helpers.RecyclerItemClickListener;


/**
 * A simple {@link Fragment} subclass.
 */
public class NavigationDrawerFragment extends Fragment {

    private ActionBarDrawerToggle mDrawerToggle;
    private DrawerLayout mDrawerLayout;
    private Toolbar mToolbar;
    private RecyclerView mRecycleView;
    private RecyclerView.Adapter mAdapter;
    private RecyclerView.LayoutManager mLayoutManager;
    private FragmentDrawerListener mDrawerListener;
    private View containerView;

    private int ICONS[] = {R.drawable.ic_home, R.drawable.ic_movies, R.drawable.ic_favorits, R.drawable.ic_events, R.drawable.ic_ajustes};
    private String TITLES[] = {"Peliculas", "Home", "Favoritos", "Horarios", "Ajustes"};
    private String NAME = "Luis Gàlvez";
    private String EMAIL = "luiscarlosg_12@hotmail.com";
    private int PROFILE = R.mipmap.ic_profile;


    public NavigationDrawerFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, final ViewGroup container,
                             Bundle savedInstanceState) {
        View  v = inflater.inflate(R.layout.fragment_navigation_drawer, container, false);

        ManejadorUsuario  mj= ManejadorUsuario.getInstancia();

        for(int posicion=0;posicion<mj.obtenerUsuarios().size();posicion++) {
            NAME = mj.obtenerUsuarios().get(posicion).getNombre();
            EMAIL = mj.obtenerUsuarios().get(posicion).getCorreo();
        }

        mRecycleView = (RecyclerView) v.findViewById(R.id.RecyclerView);
        mRecycleView.setHasFixedSize(true);
        mLayoutManager = new LinearLayoutManager(getActivity());
        mRecycleView.setLayoutManager(mLayoutManager);
        mAdapter = new NavigationDrawerAdapter(TITLES, ICONS, NAME, EMAIL, PROFILE);
        mRecycleView.setAdapter(mAdapter);

        mRecycleView.addOnItemTouchListener(new RecyclerItemClickListener(getActivity(), new RecyclerItemClickListener.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {
              mDrawerListener.onDrawerItemSelected(view, position);
                mDrawerLayout.closeDrawer(containerView);
            }
        }));
        return v;
    }


    public void setUp(DrawerLayout drawerLayout, Toolbar toolbar, int fragmentId) {
    this.mDrawerLayout = drawerLayout;
    this.mToolbar = toolbar;
    this.containerView = getActivity().findViewById(fragmentId);

    mDrawerToggle = new ActionBarDrawerToggle(getActivity(), drawerLayout, toolbar, R.string.drawer_open, R.string.drawer_close){


        @Override
        public void onDrawerOpened(View drawerView) {
            super.onDrawerOpened(drawerView);
            getActivity().invalidateOptionsMenu();
        }

        @Override
        public void onDrawerClosed(View drawerView) {
            super.onDrawerClosed(drawerView);
            getActivity().invalidateOptionsMenu();
        }

        @Override
        public void onDrawerSlide(View drawerView, float slideOffset) {
            super.onDrawerSlide(drawerView, slideOffset);
            mToolbar.setAlpha(1 - slideOffset / 2);
        }
    };

        mDrawerLayout.setDrawerListener(mDrawerToggle);
        mDrawerLayout.post(new Runnable() {
            @Override
            public void run() {
                mDrawerToggle.syncState();
            }
        });
    }

    public void setDrawerListener(FragmentDrawerListener drawerListener){
        this.mDrawerListener =  drawerListener;
    }

    public interface FragmentDrawerListener {
        public void onDrawerItemSelected(View view, int position);
    }
}