package com.example.luisgalvez.loginlc;

/**
 * Created by LuisGalvez on 21/05/2015.
 */
public class Peliculas {

    public int id;
    public String titulo;
    public String sinopsis;
    public String trailer_url;
    public String image;
    public String rated;
    public String genero;
    public static Peliculas instancia;

    public static Peliculas getInstancia(){
        if(instancia==null){
            instancia= new Peliculas();
        }
        return  instancia;
    }

    public Peliculas(int id, String titulo, String sinopsis, String trailer_url, String image, String rated, String genero) {
        this.id = id;
        this.titulo = titulo;
        this.sinopsis = sinopsis;
        this.trailer_url = trailer_url;
        this.image = "";
        this.rated = rated;
        this.genero = genero;
    }

    public Peliculas() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitulo() {
        return titulo;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    public String getSinopsis() {
        return sinopsis;
    }

    public void setSinopsis(String sinopsis) {
        this.sinopsis = sinopsis;
    }

    public String getTrailer_url() {
        return trailer_url;
    }

    public void setTrailer_url(String trailer_url) {
        this.trailer_url = trailer_url;
    }

    public String getImage() {
        return "";
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getRated() {
        return rated;
    }

    public void setRated(String rated) {
        this.rated = rated;
    }

    public String getGenero() {
        return genero;
    }

    public void setGenero(String genero) {
        this.genero = genero;
    }
}
