package com.example.luisgalvez.loginlc;

/**
 * Created by LuisGalvez on 21/05/2015.
 */
public class Usuario {

    private String nombreUsuario;

    private String pass;

    private String nombre;

    private String correo;

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getCorreo() {
        return correo;
    }

    public void setCorreo(String correo) {
        this.correo = correo;
    }

    public String getNombreUsuario() {
        return nombreUsuario;
    }

    public void setNombreUsuario(String nombreUsuario) {
        this.nombreUsuario = nombreUsuario;
    }

    public String getPass() {
        return pass;
    }

    public void setPass(String pass) {
        this.pass = pass;
    }


    public Usuario(String nombreUsuario ,String pass,String nombre, String correo){
        this.nombreUsuario = nombreUsuario;

        this.pass = pass;
        this.nombre = nombre;
        this.correo = correo;
    }

}

