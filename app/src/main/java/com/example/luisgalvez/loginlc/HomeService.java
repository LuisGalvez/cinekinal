package com.example.luisgalvez.loginlc;

import java.util.List;

import retrofit.Callback;
import retrofit.http.GET;

public interface HomeService {

    @GET("/prestrenos")
    void getPrestreno(Callback<List<Prestrenos>> callback);
}

