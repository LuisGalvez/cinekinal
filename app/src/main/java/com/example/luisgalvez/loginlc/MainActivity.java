package com.example.luisgalvez.loginlc;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.app.FragmentManager;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.preference.Preference;
import android.support.v4.app.Fragment;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.Toast;


import com.parse.LogInCallback;
import com.parse.Parse;
import com.parse.ParseACL;
import com.parse.ParseException;
import com.parse.ParseUser;

import java.util.Random;


public class MainActivity extends ActionBarActivity {

    Button btnLogin, btnRegistrar;
    EditText txtUsuarios, txtPassw;
    private CheckBox chbRememberMe;
    public SQLiteDatabase sqLiteDatabase;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        try{
            Parse.initialize(this, "PPGvgEiEBXGQAzMA9i7vVkUHAUnMjn4YG44DO3FN", "K9CTneFqxbR1U3IxfxfIytYJCMomp0ZUrO0uReOl");

            ParseUser.enableAutomaticUser();
            ParseACL defaultACL = new ParseACL();

            // If you would like all objects to be private by default, remove this
            // line.
            defaultACL.setPublicReadAccess(true);

            ParseACL.setDefaultACL(defaultACL, true);
        }catch (IllegalStateException e){

        }



        ParseUser currentUser = ParseUser.getCurrentUser();

        if (currentUser== null) {
            Intent intent = new Intent(MainActivity.this, pelicula.class);
            startActivity(intent);
            finish();
        }


        btnLogin = (Button) findViewById(R.id.btnLogin);
        btnRegistrar = (Button) findViewById(R.id.btnRegistrar);
        txtUsuarios = (EditText) findViewById(R.id.txtUsuario);
        txtPassw = (EditText) findViewById(R.id.txtPass);
        chbRememberMe = (CheckBox) findViewById(R.id.ChbRememberme);

        btnRegistrar.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                try {
                    Intent myIntent = new Intent(view.getContext(), registrar.class);
                    startActivityForResult(myIntent, 0);
                } catch (NullPointerException e) {

                }

            }
        });


        btnLogin.setOnClickListener(new View.OnClickListener() {

            public void onClick(View view) {
                if (txtPassw.getText().length()==0 && txtUsuarios.getText().length()==0){
                    Toast t = Toast.makeText(getApplicationContext(), "Ingrese bien los datos", Toast.LENGTH_LONG);
                    t.show();
                }else{
                    String usuario = txtUsuarios.getText().toString();
                    String pass = txtPassw.getText().toString();
                    ParseUser.logInInBackground(usuario, pass,
                            new LogInCallback() {
                                public void done(ParseUser user, ParseException e) {

                                    if (user != null) {
                                        // If user exist and authenticated, send user to Welcome.class
                                        Intent intent = new Intent(
                                                MainActivity.this,
                                                pelicula.class);
                                        startActivity(intent);
                                        Toast.makeText(getApplicationContext(),
                                                "Successfully Logged in",
                                                Toast.LENGTH_LONG).show();
                                        finish();
                                    } else {
                                        Toast.makeText(
                                                getApplicationContext(),
                                                "No such user exist, please signup",
                                                Toast.LENGTH_LONG).show();
                                    }
                                }
                            });
                }
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

}